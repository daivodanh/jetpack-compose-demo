package com.example.jetpackcomposedemo.model

data class Electronic(
    val name: String,
    val imgUrl: String,
)

fun mockDataElectronic(): List<Electronic>{
    val list = mutableListOf<Electronic>()
    list.add(Electronic("limber","https://i.dummyjson.com/data/products/1/thumbnail.jpg"))
    list.add(Electronic("imposter","https://i.dummyjson.com/data/products/2/thumbnail.jpg"))
    list.add(Electronic("ditto","https://i.dummyjson.com/data/products/5/thumbnail.jpg"))
    list.add(Electronic("metal-powde","https://i.dummyjson.com/data/products/4/1.jpg"))
    list.add(Electronic("quick-powder","https://i.dummyjson.com/data/products/6/thumbnail.png"))
    list.add(Electronic("transform","https://i.dummyjson.com/data/products/6/1.png"))
    return list
}
