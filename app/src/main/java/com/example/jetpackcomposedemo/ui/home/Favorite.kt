package com.example.jetpackcomposedemo.ui.home

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.AlignmentLine
import androidx.compose.ui.layout.FirstBaseline
import androidx.compose.ui.layout.layout
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
@Preview
@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun FavoriteScreen() {
    Surface(color = MaterialTheme.colors.background) {
        val dataItems = (0..100).map {
            if (it % 2 == 0) {
                ItemTypeOne("How easy was that!")
            } else ItemTypeTwo("Insanely easy", "See you later xml")
        }
        Scaffold(
            topBar = { TopAppBar(title = { Text("MultipleItemType ", style = TextStyle(color = Color.White, fontWeight = FontWeight.Bold, fontSize = 18.sp)) },
                backgroundColor = Color.Blue) },
            content = {
                MyMultipleItemList(
                    modifier = Modifier.fillMaxSize(),
                    dataItems
                )
            }
        )
    }
}

sealed class DataItem
data class ItemTypeOne(val text: String) : DataItem()
data class ItemTypeTwo(
    val text: String,
    val description: String
) : DataItem()

// multiple type trong list
@Composable
fun MyMultipleItemList(
    modifier: Modifier = Modifier,
    dataItems: List<DataItem>,
) {
    LazyColumn(modifier = modifier) {
        items(dataItems) { data ->
            when (data) {
                is ItemTypeOne -> ItemOne(itemTypeOne = data)
                is ItemTypeTwo -> ItemTwo(itemTypeTwo = data)
            }
        }
    }
}

@Composable
fun ItemOne(itemTypeOne: ItemTypeOne) {
    Text(text = itemTypeOne.text, Modifier.background(Color.Red))
}

@Composable
fun ItemTwo(itemTypeTwo: ItemTypeTwo) {
    Column(modifier = Modifier.background(Color.Yellow)) {
        Text(text = itemTypeTwo.text)
        Text(text = itemTypeTwo.description)
    }
}

fun Modifier.firstBaselineToTop(
    firstBaselineToTop: Dp
) = layout { measurable, constraints ->
    // Measure the composable
    val placeable = measurable.measure(constraints)

    // Check the composable has a first baseline
    check(placeable[FirstBaseline] != AlignmentLine.Unspecified)
    val firstBaseline = placeable[FirstBaseline]

    // Height of the composable with padding - first baseline
    val placeableY = firstBaselineToTop.roundToPx() - firstBaseline
    val height = placeable.height + placeableY
    layout(placeable.width, height) {
        // Where the composable gets placed
        placeable.placeRelative(0, placeableY)
    }
}



