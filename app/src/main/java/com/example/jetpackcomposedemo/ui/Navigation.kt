package com.example.jetpackcomposedemo.ui

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.jetpackcomposedemo.ui.home.chat.ChatScreen
import com.example.jetpackcomposedemo.ui.home.FavoriteScreen
import com.example.jetpackcomposedemo.ui.home.HomeScreen
import com.example.jetpackcomposedemo.ui.home.ProfileScreen

@Composable
fun Navigation(navController: NavHostController) {
    NavHost(navController = navController, startDestination = "home") {
        composable("home") {
            HomeScreen(navController)
        }
        composable("chat") {
            ChatScreen()
        }
        composable("profile") {
            ProfileScreen()
        }
        composable("favorite") {
            FavoriteScreen()
        }
//        composable("detail/{name}") { navBackStackEntry ->
//            val name = navBackStackEntry.arguments?.getString("name")
//            if (name == null) {
//                Toast.makeText(ctx, "Name is require", Toast.LENGTH_SHORT)
//            } else {
//                DetailScreen(name = name)
//            }
//        }
    }
}
