package com.example.jetpackcomposedemo.ui.home

import android.content.Intent
import androidx.annotation.StringRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import com.example.jetpackcomposedemo.R
import com.example.jetpackcomposedemo.model.Electronic
import com.example.jetpackcomposedemo.model.mockDataElectronic
import com.example.jetpackcomposedemo.ui.detail.DetailActivity
import java.util.*

@Composable
fun HomeScreen(navHostController: NavHostController,modifier: Modifier = Modifier) {
    Column(modifier) {
        val textState = remember {
            mutableStateOf(TextFieldValue(""))
        }
        Spacer(modifier = Modifier.height(16.dp))
        SearchBar(textState)
        HomeSection(title = R.string.home_electronic_title) {
            AlignYourBodyRow(state = textState, navHostController)
        }
        HomeSection(title = R.string.home_electronic_body) {
            FavoriteCollectionsGrid()
        }
    }
}

@Composable
fun SearchBar(state: MutableState<TextFieldValue>, modifier: Modifier = Modifier) {
    TextField(value = state.value, onValueChange = { value ->
        state.value = value
    },
        leadingIcon = {
            Icon(imageVector = Icons.Default.Search, contentDescription = null)
        },
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = MaterialTheme.colors.surface
        ),
        trailingIcon = {
            if (state.value != TextFieldValue("")) {
                IconButton(onClick = { state.value = TextFieldValue("") }) {
                    Icon(imageVector = Icons.Default.Close, contentDescription = "",
                        modifier = Modifier
                            .padding(15.dp)
                            .size(24.dp)
                    )
                }
            }
        },
        singleLine = true,
        placeholder = {
            Text(text = stringResource(id = R.string.home_search))
        },
        modifier = modifier
            .fillMaxWidth()
            .heightIn(min = 56.dp)
            .padding(horizontal = 16.dp)
    )
}

@Composable
fun AlignYourBodyElement(
    imgUrl: String,
    text: String,
    item: Electronic,
    navHostController: NavHostController,
    modifier: Modifier = Modifier,
) {
    val context = LocalContext.current
    Column(modifier = modifier.clickable {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra(DetailActivity.NAME, item.name)
        context.startActivity(intent)
//        navHostController.navigate(MainActivity.Destination.Detail.createRoute(item.name))
                                         },
        horizontalAlignment = Alignment.CenterHorizontally) {
        AsyncImage(model = imgUrl, contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .size(88.dp)
                .clip(CircleShape))
        Text(text = text,
            style = MaterialTheme.typography.body1,
            modifier = Modifier.paddingFromBaseline(
                top = 24.dp, bottom = 8.dp
            ))

    }
}

@Composable
fun FavoriteCollectionCard(
    imgUr: String,
    text: String,
    modifier: Modifier = Modifier
) {
    Surface(shape = MaterialTheme.shapes.small,
        modifier = modifier) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.width(216.dp)
        ) {
            AsyncImage(model = imgUr, contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier.size(56.dp))
            Text(text = text,
                style = MaterialTheme.typography.body2,
                modifier = Modifier.padding(horizontal = 16.dp))
        }
    }
}

// list ngang
@Composable
fun AlignYourBodyRow(state: MutableState<TextFieldValue>,navHostController: NavHostController, modifier: Modifier = Modifier) {
    var filteredItems: List<Electronic>
    LazyRow(
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        contentPadding = PaddingValues(horizontal = 16.dp),
        modifier = modifier) {
        val searchedText = state.value.text
        filteredItems = if (searchedText.isEmpty()) {
            mockDataElectronic()
        } else {
            val resultList = ArrayList<Electronic>()
            for (item in mockDataElectronic()) {
                if (item.name.lowercase(Locale.getDefault()).contains(searchedText.lowercase(Locale.getDefault()))) {
                    resultList.add(item)
                }
            }
            resultList
        }
        items(filteredItems) { item ->
            AlignYourBodyElement(imgUrl = item.imgUrl, text = item.name, item, navHostController)
        }
    }
}

// list dạng cột
@Composable
fun FavoriteCollectionsGrid(modifier: Modifier = Modifier) {
    LazyHorizontalGrid(
        //gridcell truyền fixed count giống span count của grid view
        rows = GridCells.Fixed(2),
        contentPadding = PaddingValues(horizontal = 16.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp),
        modifier = modifier.height(140.dp)
    ) {
        items(mockDataElectronic()) { item ->
            FavoriteCollectionCard(imgUr = item.imgUrl, text = item.name, modifier = Modifier.height(56.dp))
        }
    }
}

@Composable
fun HomeSection(
    @StringRes title: Int,
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    Column(modifier) {
        Text(text = stringResource(id = title).uppercase(Locale.getDefault()),
            style = MaterialTheme.typography.h6,
            modifier = Modifier
                .paddingFromBaseline(top = 28.dp, bottom = 16.dp)
                .padding(horizontal = 16.dp))
        content()
    }
}


