package com.example.jetpackcomposedemo.ui.detail

import android.os.Bundle

import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent

class DetailActivity: ComponentActivity() {
    companion object{
        const val NAME = "name"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val name = intent.getStringExtra(NAME)
        setContent{
            DetailScreen(name = name.toString())
        }
    }
}
