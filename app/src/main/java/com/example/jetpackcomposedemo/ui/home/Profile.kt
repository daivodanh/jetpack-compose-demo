package com.example.jetpackcomposedemo.ui.home

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import kotlin.math.roundToInt

@Composable
fun ProfileScreen() {
    Box(modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center) {
        GesturesDemo()
    }
}

@Preview
@Composable
fun GesturesDemo() {
    val count = remember { mutableStateOf(0) }
    val pressCount = remember {
        mutableStateOf(0)
    }

    Column {
        Button(onClick = {
            count.value++
        }) {
            Text("Click me")
        }

        if (count.value < 3) {
            SideEffect {
                Log.d("Compose", "onactive with value: " + count.value)
            }
        }
        DisposableEffect(Unit) {
            onDispose {
                Log.d("Compose", "onDispose because value=" + count.value)
            }
        }
        Text(text = "You have clicked the button: " + count.value.toString())
        Surface(modifier = Modifier.padding(16.dp)) {
        }
        Text(text = "Please press to me " + pressCount.value.toString(),
            modifier = Modifier.pointerInput(Unit) {
                detectTapGestures(onDoubleTap = { pressCount.value += 1 })
            })
        DraggableTextLowLevel()
        SwipeAbleSample()
    }
}
@Preview
@Composable
fun DraggableTextLowLevel() {
    Box(modifier = Modifier.width(400.dp).height(200.dp)) {
        var offsetX by remember { mutableStateOf(0f) }
        var offsetY by remember { mutableStateOf(0f) }

        Box(
            Modifier
                .offset { IntOffset(offsetX.roundToInt(), offsetY.roundToInt()) }
                .background(Color.Blue)
                .size(50.dp)
                .pointerInput(Unit) {
                    detectDragGestures { change, dragAmount ->
                        change.consume()
                        offsetX += dragAmount.x
                        offsetY += dragAmount.y
                    }
                }
        )
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SwipeAbleSample() {
    val width = 96.dp
    val squareSize = 48.dp

    val swipeAbleState = rememberSwipeableState(0)
    val sizePx = with(LocalDensity.current) { squareSize.toPx() }
    val anchors = mapOf(0f to 0, sizePx to 1) // Maps anchor points (in px) to states

    Box(
        modifier = Modifier
            .width(width)
            .swipeable(
                state = swipeAbleState,
                anchors = anchors,
                thresholds = { _, _ -> FractionalThreshold(0.3f) },
                orientation = Orientation.Horizontal
            )
            .background(Color.Green)
    ) {
        Box(
            Modifier
                .offset { IntOffset(swipeAbleState.offset.value.roundToInt(), 0) }
                .size(squareSize)
                .background(Color.DarkGray)
        )
    }
}

//fun Modifier.pad
