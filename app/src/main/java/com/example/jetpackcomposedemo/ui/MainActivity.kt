package com.example.jetpackcomposedemo.ui

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Notifications
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.jetpackcomposedemo.model.BottomNavItem
import com.example.jetpackcomposedemo.ui.theme.JetpackComposeDemoTheme

class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterialScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            JetpackComposeDemoTheme {
                Box() {
                    val navController = rememberNavController()
                    Scaffold(
                        bottomBar = {
                            BottomNavigationBar(items = initBottomNavItem(), navController = navController,
                                onItemClick = {
                                    navController.navigate(it.route) {
                                        navController.graph.startDestinationRoute?.let { route ->
                                            popUpTo(route) {
                                                saveState = true
                                            }
                                        }
                                        launchSingleTop = true
                                        restoreState = true
                                    }
                                })
                        },
                        backgroundColor = Color(0xFFF0EAE2)
                    ) {
                        Navigation(navController = navController)
                    }
                }
            }
        }
    }

    @Composable
    fun BottomNavigationBar(
        items: List<BottomNavItem>,
        navController: NavController,
        modifier: Modifier = Modifier,
        onItemClick: (BottomNavItem) -> Unit
    ) {
        val backStackEntry = navController.currentBackStackEntryAsState()
        BottomNavigation(
            modifier = modifier,
            backgroundColor = Color.DarkGray,
            elevation = 5.dp) {
            items.forEach { item ->
                val selected = item.route == backStackEntry.value?.destination?.route
                BottomNavigationItem(
                    selected = selected,
                    onClick = { onItemClick(item) },
                    selectedContentColor = Color.Green,
                    unselectedContentColor = Color.Gray,
                    icon = {
                        Icon(imageVector = item.icon, contentDescription = item.name)
                    })
            }
        }
    }

    private fun initBottomNavItem(): List<BottomNavItem> {
        val list = mutableListOf<BottomNavItem>()
        val list1 = BottomNavItem(name = "Home", route = "home", icon = Icons.Default.Home)
        list.add(list1)
        val list2 = BottomNavItem(name = "Chat", route = "chat", icon = Icons.Default.Notifications)
        list.add(list2)
        val list3 = BottomNavItem(name = "Profile", route = "profile", icon = Icons.Default.Person)
        list.add(list3)
        val list4 = BottomNavItem(name = "Favorite", route = "favorite", icon = Icons.Filled.Favorite)
        list.add(list4)
        return list
    }


    @Preview(showBackground = true)
    @Composable
    fun DefaultPreview() {
        JetpackComposeDemoTheme {

        }
    }
}
